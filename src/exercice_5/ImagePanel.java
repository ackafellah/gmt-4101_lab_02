package exercice_5;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import javax.swing.JPanel;

public class ImagePanel extends JPanel implements Serializable {
    Image image = null;
    
    /**
	    Constructeur de l'ImagePanel (JPanel designed pour une image)
	    @param a Le premier nombre entier.
	    @param b Le deuxi�me nombre entier.
	    @return La valeur de la somme des deux entiers sp�cifi�s.
	*/
    public ImagePanel(Image image) {
        this.image = image;
    }
    public ImagePanel() {
    }
    public void setImage(Image image){
        this.image = image;
    }
    public Image getImage(Image image){
        return image;
    }
    
	/**
	    Affichage d'une image � partir de sa taille r�elle.
	    @param g Graphique pour l'affichage.
	*/
    public void paintComponent(Graphics g) {
        super.paintComponent(g); //paint background
        if (image != null) { //there is a picture: draw it
            int height = this.getSize().height;
            int width = this.getSize().width;
            g.drawImage(image, 0, 0, this); //use image size          
            //g.drawImage(image,0,0, width, height, this);
        }
    }
    
	/**  
	 * Redimensionne une image. 
	 *  
	 * @param source Image � redimensionner. 
	 * @param width Largeur de l'image cible. 
	 * @param height Hauteur de l'image cible. 
	 * @return Image redimensionn�e. 
	 */ 
	public static Image scale(Image source, int width, int height) { 
	    /* On cr�e une nouvelle image aux bonnes dimensions. */ 
	    BufferedImage buf = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB); 
	  
	    /* On dessine sur le Graphics de l'image bufferis�e. */ 
	    Graphics2D g = buf.createGraphics(); 
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR); 
	    g.drawImage(source, 0, 0, width, height, null); 
	    g.dispose(); 
	  
	    /* On retourne l'image bufferis�e, qui est une image. */ 
	    return buf; 
	}
}